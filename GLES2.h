#ifndef CTEST_GL_H
#define CTEST_GL_H

#ifdef __APPLE__
    #include <OpenGLES/ES2/gl.h>
#elif __ANDROID__
    #include <GLES2/gl2.h>
#endif

#endif //CTEST_GL_H
