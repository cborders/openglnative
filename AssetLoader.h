#ifndef MOO_ASSETLOADER_H
#define MOO_ASSETLOADER_H

#include <string.h>
#include <string>

#include "Render/Data/Texture.h"

namespace Moo {
    class AssetLoader {
    public:
        AssetLoader(const void *environment);

        const char * loadShaderFile(const std::string &fileName);
        Texture loadTexture(const std::string fileName, bool hasAlpha);

    private:
        const void *_environment;
    };
}

#endif //MOO_ASSETLOADER_H
