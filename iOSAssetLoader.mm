#include "AssetLoader.h"

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#include "Logger.h"

namespace Moo {
    Moo::Logger logger;

    AssetLoader::AssetLoader(const void *environment)
    {
        
    }

    const char * AssetLoader::loadShaderFile(const std::string &fileName)
    {
        NSString *nsFileName = [NSString stringWithCString:fileName.c_str() encoding:[NSString defaultCStringEncoding]];
        NSArray *parts = [nsFileName componentsSeparatedByString:@"."];
        NSString *path = [[NSBundle mainBundle] pathForResource:parts[0] ofType:parts[1]];
        NSString *contents = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        return contents.UTF8String;
    }
    
    Texture AssetLoader::loadTexture(const std::string fileName, bool hasAlpha)
    {
        NSString *nsFileName = [NSString stringWithCString:fileName.c_str() encoding:[NSString defaultCStringEncoding]];
        NSArray *parts = [nsFileName componentsSeparatedByString:@"."];
        NSString *path = [[NSBundle mainBundle] pathForResource:parts[0] ofType:parts[1]];
        
        if (path == nil)
        {
            logger.print("Failed to load texture %", fileName);
            return Moo::Texture::errorTexture();
        }
        
        NSData *imageData = [NSData dataWithContentsOfFile:path];
        UIImage *image = [[UIImage alloc] initWithData:imageData];
        
        CFDataRef pixelData = CGDataProviderCopyData(CGImageGetDataProvider(image.CGImage));
        
        // TODO: If this image is supposed to have alpha we can just use the pixels directly
        const uint8_t* pixels = CFDataGetBytePtr(pixelData);
        
        Moo::Texture texture;
        
        if(hasAlpha)
        {
            texture = Moo::Texture::loadTexture(pixels, image.size.width, image.size.height, hasAlpha);
        }
        else
        {
            int bytesPerPixel = 3;
            uint8_t* rgb = (uint8_t*)malloc(image.size.width * image.size.height * bytesPerPixel);
            
            for(int i = 0; i < image.size.width * image.size.height; i++)
            {
                rgb[i * bytesPerPixel + 0] = pixels[i * 4 + 0];
                rgb[i * bytesPerPixel + 1] = pixels[i * 4 + 1];
                rgb[i * bytesPerPixel + 2] = pixels[i * 4 + 2];
            }
            
            texture = Moo::Texture::loadTexture(rgb, image.size.width, image.size.height, hasAlpha);

            delete[] rgb;
        }
        
        CFRelease(pixelData);
        
        return texture;
    }
}
