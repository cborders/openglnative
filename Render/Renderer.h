#ifndef CTEST_RENDERER_H
#define CTEST_RENDERER_H

#include "../AssetLoader.h"
#include "../Logger.h"
#include "ShaderLoader.h"

class Renderer {

public:
    Renderer(Moo::AssetLoader *assetLoader);
    ~Renderer();

    void initGL();
    void resize(int width, int height);
    void draw(float deltaTime);

	void touchDown(int pointerId, float x, float y);
	void touchMove(int pointerId, float x, float y);
	void touchUp(int pointerId, float x, float y);

private:
    Moo::Logger *_logger;
    Moo::ShaderLoader *_shaderLoader;
    Moo::AssetLoader *_assetLoader;

    float _width, _height;
};

#endif //CTEST_RENDERER_H
