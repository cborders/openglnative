#ifdef __APPLE__
    #include <stdlib.h>
#elif __ANDROID__
    #include <malloc.h>
#endif

#include "../Math/Matrix.h"
#include "Data/Model.h"
#include "Data/Texture.h"
#include "Renderer.h"
#include "../GLES2.h"
#include "ShaderLoader.h"

Moo::Model model = Moo::Model::makeCube(1.0f);
uint8_t pixels[] = { 255, 0, 0,
                  0, 255, 0,
                  0, 0, 255,
                  255, 255, 0 };
Moo::Texture texture;

Moo::Shader shader;
float angleX = 0.0f;
float angleY = 0.0f;

Moo::Matrix perspective;
Moo::Matrix modelView;
Moo::Matrix modelViewPerspective;

float lastTouchX, lastTouchY, frameChangeX, frameChangeY;

Renderer::Renderer(Moo::AssetLoader *assetLoader)
{
    _logger = new Moo::Logger();
    _shaderLoader = new Moo::ShaderLoader();
    _assetLoader = assetLoader;
}

Renderer::~Renderer()
{
    delete _logger;
    delete _shaderLoader;
}

void Renderer::initGL()
{
    shader.vertString = _assetLoader->loadShaderFile("shader.vert");
    shader.fragString = _assetLoader->loadShaderFile("shader.frag");

    _shaderLoader->buildShaderProgram(&shader);

//    texture = _assetLoader->loadTexture("star.png", true);
    texture = _assetLoader->loadTexture("store.jpg", false);

    glClearColor(0.51372549f, 0.5215686f, 0.5490196f, 1.0f);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void Renderer::resize(int width, int height)
{
    _width = width;
    _height = height;
}

void Renderer::draw(float deltaTime)
{
    float aspect = _width / _height;
    perspective.loadIdentity();
    perspective.perspective(60.0f, aspect, 1.0f, 20.0f);
    
    glViewport(0, 0, _width, _height);
    
    GLint mvpLocation = shader.getUniform("modelViewProjectionMatrix");
    
    GLint positionLocation = shader.getAttribute("aPosition");
    GLint texCoordLocation = shader.getAttribute("aTexCoord");

    GLint textureLocation = shader.getUniform("sTexture");
    
    
    angleX += frameChangeX * deltaTime * 10;
    angleY += frameChangeY * deltaTime * 10;
    
    frameChangeX = frameChangeY = 0;
    
    modelView.loadIdentity();
    modelView.translate(0.0f, 0.0f, -3.0f);
    modelView.rotate(-angleX, 0.0f, 1.0f, 0.0f);
	modelView.rotate(-angleY, 1.0f, 0.0f, 0.0f);
    
    modelViewPerspective = modelView * perspective;
    
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(shader.program);

    glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, modelViewPerspective.asArray());

    glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, model.vertices);
    glEnableVertexAttribArray(positionLocation);
    glVertexAttribPointer(texCoordLocation, 2, GL_FLOAT, GL_TRUE, 0, model.textureCoords);
    glEnableVertexAttribArray(texCoordLocation);

    glActiveTexture(GL_TEXTURE0 + texture.textureId);
    glBindTexture(GL_TEXTURE_2D, texture.textureId);
    glUniform1i(textureLocation, 0);

    glDrawElements(GL_TRIANGLES, model.indexCount, GL_UNSIGNED_SHORT, model.indices);
}

void Renderer::touchDown(int pointerId, float x, float y)
{
	lastTouchX = x;
	lastTouchY = y;
}

void Renderer::touchMove(int pointerId, float x, float y)
{
	frameChangeX += x - lastTouchX;
	lastTouchX = x;

	frameChangeY += y - lastTouchY;
	lastTouchY = y;
}

void Renderer::touchUp(int pointerId, float x, float y)
{

}
