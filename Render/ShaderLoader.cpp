#include "ShaderLoader.h"

#ifdef __APPLE__
    #include <stdlib.h>
#elif __ANDROID__
    #include <malloc.h>
#endif

#include "../Logger.h"

namespace Moo {
    GLint Shader::getUniform(std::string name)
    {
        return _uniforms[name];
    }
    
    GLint Shader::getAttribute(std::string name)
    {
        return _attributes[name];
    }
    
    ShaderLoader::ShaderLoader()
    {
        _logger = new Logger();
    }

    ShaderLoader::~ShaderLoader()
    {
        delete _logger;
    }

    bool ShaderLoader::buildShaderProgram(Shader *shader) {
        shader->vertShader = loadShader(shader->vertString, GL_VERTEX_SHADER);
        shader->fragShader = loadShader(shader->fragString, GL_FRAGMENT_SHADER);
        shader->program = glCreateProgram();

        if (!shader->vertShader || !shader->fragShader || !shader->program) {
            return false;
        }

        if(!linkShader(shader))
        {
            return false;
        }
        
        loadUniforms(shader);
        loadAttributes(shader);

        return true;
    }

    GLuint ShaderLoader::loadShader(const char *shaderSrc, GLenum shaderType) {
        GLuint shader;
        GLint compiled;

        shader = glCreateShader(shaderType);
        if (shader == 0) { return 0; }

        glShaderSource(shader, 1, &shaderSrc, NULL);
        glCompileShader(shader);
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

        if (!compiled) {
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

            if (infoLen > 1) {
                char *infoLog = (char *) malloc(sizeof(char) * infoLen);
                glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
                _logger->println("Shader Loading Failed: %\n", infoLog);
                free(infoLog);
            }

            glDeleteShader(shader);
            return 0;
        }

        return shader;
    }
    
    bool ShaderLoader::linkShader(Shader *shader)
    {
        glAttachShader(shader->program, shader->vertShader);
        glAttachShader(shader->program, shader->fragShader);
        
        glLinkProgram(shader->program);
        
        GLint linked;
        
        glGetProgramiv(shader->program, GL_LINK_STATUS, &linked);
        
        if (!linked) {
            GLint infoLen = 0;
            
            glGetProgramiv(shader->program, GL_INFO_LOG_LENGTH, &infoLen);
            
            if (infoLen > 1) {
                char *infoLog = (char *) malloc(sizeof(char) * infoLen);
                glGetProgramInfoLog(shader->program, infoLen, NULL, infoLog);
                _logger->println("Shader Linking Failed: %\n", infoLog);
                free(infoLog);
            }
            
            glDeleteProgram(shader->program);
            return false;
        }
        
        return true;
    }
    
    void ShaderLoader::loadUniforms(Shader *shader)
    {
        GLint maxNameLength;
        GLint uniformCount;
        
        glGetProgramiv(shader->program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxNameLength);
        glGetProgramiv(shader->program, GL_ACTIVE_UNIFORMS, &uniformCount);
        
        char *name = (char *)malloc(sizeof(char) * maxNameLength);
        
        for(int i = 0; i < uniformCount; i++)
        {
            GLint size;
            GLenum type;

            glGetActiveUniform(shader->program, i, maxNameLength, NULL, &size, &type, name);
            
            GLint location = glGetUniformLocation(shader->program, name);
            
            shader->_uniforms[std::string(name)] = location;
        }
    }
    
    void ShaderLoader::loadAttributes(Shader *shader)
    {
        GLint maxNameLength;
        GLint attributeCount;
        
        glGetProgramiv(shader->program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxNameLength);
        glGetProgramiv(shader->program, GL_ACTIVE_ATTRIBUTES, &attributeCount);
        
        char *name = (char *)malloc(sizeof(char) * maxNameLength);
        
        for(int i = 0; i < attributeCount; i++)
        {
            GLint size;
            GLenum type;
            
            glGetActiveAttrib(shader->program, i, maxNameLength, NULL, &size, &type, name);
            
            GLint location = glGetAttribLocation(shader->program, name);
            
            shader->_attributes[std::string(name)] = location;
        }
    }
}
