#ifndef MOO_TEXTURE_H
#define MOO_TEXTURE_H

#include <stdint.h>

namespace Moo {
    class Texture
    {
    public:
        uint8_t *pixels;
        int width, height;
        unsigned int textureId;

        static Texture loadTexture(const uint8_t *pixels, int width, int height, bool hasAlpha);
	    static Texture errorTexture();
    };
}

#endif //MOO_TEXTURE_H
