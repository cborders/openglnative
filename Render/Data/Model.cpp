#include "Model.h"

#ifdef __APPLE__
    #include <stdlib.h>
#elif __ANDROID__
    #include <malloc.h>
#endif

#include <string.h>
#include <math.h>

namespace Moo {
    Model Model::makeCube(float scale)
    {
        float cubeVerts[] =
        {
            -0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f,  0.5f,
            0.5f, -0.5f,  0.5f,
            0.5f, -0.5f, -0.5f,
            -0.5f,  0.5f, -0.5f,
            -0.5f,  0.5f,  0.5f,
            0.5f,  0.5f,  0.5f,
            0.5f,  0.5f, -0.5f,
            -0.5f, -0.5f, -0.5f,
            -0.5f,  0.5f, -0.5f,
            0.5f,  0.5f, -0.5f,
            0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f, 0.5f,
            -0.5f,  0.5f, 0.5f,
            0.5f,  0.5f, 0.5f,
            0.5f, -0.5f, 0.5f,
            -0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f,  0.5f,
            -0.5f,  0.5f,  0.5f,
            -0.5f,  0.5f, -0.5f,
            0.5f, -0.5f, -0.5f,
            0.5f, -0.5f,  0.5f,
            0.5f,  0.5f,  0.5f,
            0.5f,  0.5f, -0.5f,
        };
        
        float cubeNormals[] =
        {
            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
        };
        
        float cubeTex[] =
        {
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,
            0.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
        };
        
        uint16_t cubeIndices[] =
        {
            0, 2, 1,
            0, 3, 2,
            4, 5, 6,
            4, 6, 7,
            8, 9, 10,
            8, 10, 11,
            12, 15, 14,
            12, 14, 13,
            16, 17, 18,
            16, 18, 19,
            20, 23, 22,
            20, 22, 21
        };

        Model model;
        model.indexCount = 36;
        model.vertexCount = 24;
        
        model.vertices = (float*)malloc (sizeof(float) * 3 * model.vertexCount);
        memcpy (model.vertices, cubeVerts, sizeof(cubeVerts));
        
        int i;
        for (i = 0; i < model.vertexCount * 3; i++)
        {
            model.vertices[i] *= scale;
        }
        
        model.normals = (float*)malloc (sizeof(float) * 3 * model.vertexCount);
        memcpy (model.normals, cubeNormals, sizeof(cubeNormals));
        
        model.textureCoords = (float*)malloc (sizeof(float) * 2 * model.vertexCount);
        memcpy (model.textureCoords, cubeTex, sizeof(cubeTex));
        
        model.indices = (uint16_t*)malloc (sizeof(uint16_t) * model.indexCount);
        memcpy (model.indices, cubeIndices, sizeof(cubeIndices));
        
        return model;
    }
    
    Model Model::makeSphere(int slices, float radius)
    {
        int parallels = slices / 2;
        float angleStep = (2.0f * M_PI) / ((float) slices);
        
        Model model;
        model.vertexCount = (parallels + 1) * (slices + 1);
        model.indexCount = parallels * slices * 6;
        
        model.vertices = (float*)malloc(sizeof(float) * 3 * model.vertexCount);
        model.normals = (float*)malloc(sizeof(float) * 3 * model.vertexCount);
        model.textureCoords = (float*)malloc(sizeof(float) * 2 * model.vertexCount);
        model.indices = (uint16_t*)malloc(sizeof(uint16_t) * model.indexCount);
        
        for (int i = 0; i < parallels + 1; i++ )
        {
            for (int j = 0; j < slices + 1; j++ )
            {
                int vertex = ( i * (slices + 1) + j ) * 3;
                model.vertices[vertex + 0] = radius * sinf (angleStep * (float)i) * sinf (angleStep * (float)j);
                model.vertices[vertex + 1] = radius * cosf (angleStep * (float)i);
                model.vertices[vertex + 2] = radius * sinf (angleStep * (float)i) * cosf (angleStep * (float)j);
                
                model.normals[vertex + 0] = model.vertices[vertex + 0] / radius;
                model.normals[vertex + 1] = model.vertices[vertex + 1] / radius;
                model.normals[vertex + 2] = model.vertices[vertex + 2] / radius;
                
                int texIndex = ( i * (slices + 1) + j ) * 2;
                model.textureCoords[texIndex + 0] = (float)j / (float)slices;
                model.textureCoords[texIndex + 1] = (1.0f - (float)i) / (float)(parallels - 1);
            }
        }
        
        uint16_t *indexBuf = model.indices;
        for (int i = 0; i < parallels ; i++)
        {
            for (int j = 0; j < slices; j++)
            {
                *indexBuf++ = i * (slices + 1) + j;
                *indexBuf++ = (i + 1) * (slices + 1) + j;
                *indexBuf++ = (i + 1) * (slices + 1) + (j + 1);
                
                *indexBuf++ = i * (slices + 1) + j;
                *indexBuf++ = (i + 1) * (slices + 1) + (j + 1);
                *indexBuf++ = i * (slices + 1) + (j + 1);
            }
        }
        
        return model;
    }
}
