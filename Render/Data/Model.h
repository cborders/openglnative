#ifndef MOO_MODEL_H
#define MOO_MODEL_H

#include <stdint.h>

namespace Moo {
    class Model
    {
    public:
        float *vertices;
        float *normals;
        float *textureCoords;
        uint16_t *indices;
        int vertexCount, indexCount;
        
        static Model makeCube(float scale);
        static Model makeSphere(int slices, float radius);
    };
}

#endif //MOO_MODEL_H
