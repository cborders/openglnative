#ifndef MOO_SHADERLOADER_H
#define MOO_SHADERLOADER_H

#include <map>
#include <string>

#include "../GLES2.h"
#include "../Logger.h"

namespace Moo {
    typedef struct {
        friend class ShaderLoader;
        const char *vertString;
        const char *fragString;
        
        GLuint vertShader;
        GLuint fragShader;
        GLuint program;
        
        GLint getUniform(std::string name);
        GLint getAttribute(std::string name);

    private:
        std::map<std::string, GLint> _uniforms;
        std::map<std::string, GLint> _attributes;
    } Shader;

    class ShaderLoader {
    public:
        ShaderLoader();
        ~ShaderLoader();

        bool buildShaderProgram(Shader *shader);

    private:
        GLuint loadShader(const char *shaderSrc, GLenum shaderType);
        bool linkShader(Shader *shader);
        void loadUniforms(Shader *shader);
        void loadAttributes(Shader *shader);

        Logger *_logger;
    };
}

#endif //MOO_SHADERLOADER_H
