#ifndef MOO_LOGGER_H
#define MOO_LOGGER_H

#include <iostream>
#include <stdexcept>
#include <stdio.h>

namespace Moo
{
    class Logger
    {
        std::streambuf *_oldBuf;
        
    public:
        Logger();
        Logger(std::streambuf *output);
        ~Logger();
        
        void print(const char* s);
        void println(const char* s);
        
        template<typename T, typename... Args>
        void print(const char* s, const T &value, Args... args)
        {
            while (s && *s)
            {
                if (*s=='%' && *s+1!='%')
                {
                    std::clog << value;
                    return print(++s, args...);
                }
                std::clog << *s++;
            }
            throw std::runtime_error("extra arguments provided to Logger::print");
        }
        
        template<typename T, typename... Args>
        void println(const char* s, const T &value, Args... args)
        {
            while (s && *s)
            {
                if (*s=='%' && *s+1!='%')
                {
                    std::clog << value;
                    return println(++s, args...);
                }
                std::clog << *s++;
            }
            throw std::runtime_error("extra arguments provided to Logger::println");
        }
    };
}
#endif //MOO_LOGGER_H
