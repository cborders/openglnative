#ifndef MOO_MATRIX_H
#define MOO_MATRIX_H

#include "../GLES2.h"

namespace Moo {
    class Matrix
    {
    public:
        void loadIdentity();
        
        void scale(float x, float y, float z);
        void translate(float x, float y, float z);
        void rotate(float angle, float x, float y, float z);
        
        void frustum(float left, float right, float bottom, float top, float near, float far);
        void perspective(float fovY, float aspect, float near, float far);
        void ortho(float left, float right, float bottom, float top, float near, float far);
        
        float* asArray();
        
        Matrix operator*(const Matrix &other);
        Matrix& operator=(const Matrix &other);
        Matrix& operator*=(const Matrix &other);

    private:
        GLfloat _matrix[4][4];
    };
}

#endif //MOO_MATRIX_H
