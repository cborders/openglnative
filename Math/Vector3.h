#ifndef MOO_VECTOR3_H
#define MOO_VECTOR3_H

namespace Moo {
    class Vector3
    {
    public:
        Vector3();
        Vector3(float scaler);
        Vector3(float x, float y, float z);
        
        float length();
        float lengthSquared();
        
        float distance(const Vector3& other);
        float distanceSquared(const Vector3& other);
        
        float dot(const Vector3& other);
        Vector3 cross(const Vector3& other);
        
        void normalize();
        
        Vector3 operator+(const Vector3& other);
        Vector3 operator-(const Vector3& other);
        Vector3 operator*(const Vector3& other);
        Vector3 operator/(const Vector3& other);
        
        Vector3 operator*(const float scaler);
        Vector3 operator/(const float scaler);
        
        Vector3& operator+=(const Vector3& other);
        Vector3& operator-=(const Vector3& other);
        Vector3& operator*=(const Vector3& other);
        Vector3& operator/=(const Vector3& other);
        
        Vector3& operator*=(const float scaler);
        Vector3& operator/=(const float scaler);
        
        union
        {
            struct { float x, y, z; };
            float members[3];
        };
    };
}

#endif //MOO_VECTOR3_H
