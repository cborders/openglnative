#include "Vector3.h"

#include <math.h>

namespace Moo {
    Vector3::Vector3() { }
    Vector3::Vector3(float scaler) : x(scaler), y(scaler), z(scaler) { }
    Vector3::Vector3(float x, float y, float z) : x(x), y(y), z(z) { }
    
    float Vector3::length()
    {
        return sqrt (x * x + y * y + z * z);
    }
    
    float Vector3::lengthSquared()
    {
        return x * x + y * y + z * z;
    }
    
    float Vector3::distance(const Vector3& other)
    {
        return (*this - other).length();
    }
    
    float Vector3::distanceSquared(const Vector3& other)
    {
        return (*this - other).lengthSquared();
    }
    
    float Vector3::dot(const Vector3& other)
    {
        return x * other.x + y * other.y + z * other.z;
    }
    
    Vector3 Vector3::cross(const Vector3& other)
    {
        return Vector3 (y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x);
    }
    
    void Vector3::normalize()
    {
        float length = this->length();
        if (length > 0)
        {
            float inverseLength = 1.0f / length;
            x *= inverseLength;
            y *= inverseLength;
            z *= inverseLength;
        }
    }
    
    Vector3 Vector3::operator+(const Vector3& other)
    {
        return Vector3(x + other.x, y + other.y, z + other.z);
    }
    
    Vector3 Vector3::operator-(const Vector3& other)
    {
        return Vector3(x - other.x, y - other.y, z - other.z);
    }
    
    Vector3 Vector3::operator*(const Vector3& other)
    {
        return Vector3(x * other.x, y * other.y, z * other.z);
    }
    
    Vector3 Vector3::operator/(const Vector3& other)
    {
        return Vector3(x / other.x, y / other.y, z / other.z);
    }
    
    Vector3 Vector3::operator*(const float scaler)
    {
        return Vector3(x * scaler, y * scaler, z * scaler);
    }
    
    Vector3 Vector3::operator/(const float scaler)
    {
        return Vector3(x / scaler, y / scaler, z / scaler);
    }
    
    Vector3& Vector3::operator+=(const Vector3& other)
    {
        x += other.x;
        y += other.y;
        z += other.z;
        return *this;
    }
    
    Vector3& Vector3::operator-=(const Vector3& other)
    {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        return *this;
    }
    
    Vector3& Vector3::operator*=(const Vector3& other)
    {
        x *= other.x;
        y *= other.y;
        z *= other.z;
        return *this;
    }
    
    Vector3& Vector3::operator/=(const Vector3& other)
    {
        x /= other.x;
        y /= other.y;
        z /= other.z;
        return *this;
    }
    
    Vector3& Vector3::operator*=(const float scaler)
    {
        x *= scaler;
        y *= scaler;
        z *= scaler;
        return *this;
    }
    
    Vector3& Vector3::operator/=(const float scaler)
    {
        x /= scaler;
        y /= scaler;
        z /= scaler;
        return *this;
    }
}
