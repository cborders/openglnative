#include "Matrix.h"

#include <math.h>
#include <string.h>

namespace Moo {
    void Matrix::loadIdentity()
    {
        memset(_matrix, 0, sizeof(Matrix));
        _matrix[0][0] = 1.0f;
        _matrix[1][1] = 1.0f;
        _matrix[2][2] = 1.0f;
        _matrix[3][3] = 1.0f;
    }
    
    void Matrix::scale(float x, float y, float z)
    {
        _matrix[0][0] *= x;
        _matrix[0][1] *= x;
        _matrix[0][2] *= x;
        _matrix[0][3] *= x;
        
        _matrix[1][0] *= y;
        _matrix[1][1] *= y;
        _matrix[1][2] *= y;
        _matrix[1][3] *= y;
        
        _matrix[2][0] *= z;
        _matrix[2][1] *= z;
        _matrix[2][2] *= z;
        _matrix[2][3] *= z;
    }
    
    void Matrix::translate(float x, float y, float z)
    {
        _matrix[3][0] += (_matrix[0][0] * x + _matrix[1][0] * y + _matrix[2][0] * z);
        _matrix[3][1] += (_matrix[0][1] * x + _matrix[1][1] * y + _matrix[2][1] * z);
        _matrix[3][2] += (_matrix[0][2] * x + _matrix[1][2] * y + _matrix[2][2] * z);
        _matrix[3][3] += (_matrix[0][3] * x + _matrix[1][3] * y + _matrix[2][3] * z);
    }
    
    void Matrix::rotate(float angle, float x, float y, float z)
    {
        float mag = sqrtf(x * x + y * y + z * z);

        if (mag > 0.0f)
        {
            float sinAngle = sinf (angle * M_PI / 180.0f);
            float cosAngle = cosf (angle * M_PI / 180.0f);

            x /= mag;
            y /= mag;
            z /= mag;
            
            float xx = x * x;
            float yy = y * y;
            float zz = z * z;
            float xy = x * y;
            float yz = y * z;
            float zx = z * x;
            float xs = x * sinAngle;
            float ys = y * sinAngle;
            float zs = z * sinAngle;
            float oneMinusCos = 1.0f - cosAngle;
            
            Matrix rotation;
            rotation._matrix[0][0] = (oneMinusCos * xx) + cosAngle;
            rotation._matrix[0][1] = (oneMinusCos * xy) - zs;
            rotation._matrix[0][2] = (oneMinusCos * zx) + ys;
            rotation._matrix[0][3] = 0.0F;
            
            rotation._matrix[1][0] = (oneMinusCos * xy) + zs;
            rotation._matrix[1][1] = (oneMinusCos * yy) + cosAngle;
            rotation._matrix[1][2] = (oneMinusCos * yz) - xs;
            rotation._matrix[1][3] = 0.0F;
            
            rotation._matrix[2][0] = (oneMinusCos * zx) - ys;
            rotation._matrix[2][1] = (oneMinusCos * yz) + xs;
            rotation._matrix[2][2] = (oneMinusCos * zz) + cosAngle;
            rotation._matrix[2][3] = 0.0F;
            
            rotation._matrix[3][0] = 0.0F;
            rotation._matrix[3][1] = 0.0F;
            rotation._matrix[3][2] = 0.0F;
            rotation._matrix[3][3] = 1.0F;

            *this = rotation * *this;
        }
    }
    
    void Matrix::frustum(float left, float right, float bottom, float top, float near, float far)
    {
        float width = right - left;
        float height = top - bottom;
        float depth = far - near;

        if (near <= 0.0f || far <= 0.0f || width <= 0.0f || height <= 0.0f || depth <= 0.0f) { return; }
        
        Matrix frustum;
        frustum._matrix[0][0] = 2.0f * near / width;
        frustum._matrix[0][1] = frustum._matrix[0][2] = frustum._matrix[0][3] = 0.0f;
        
        frustum._matrix[1][1] = 2.0f * near / height;
        frustum._matrix[1][0] = frustum._matrix[1][2] = frustum._matrix[1][3] = 0.0f;
        
        frustum._matrix[2][0] = (right + left) / width;
        frustum._matrix[2][1] = (top + bottom) / height;
        frustum._matrix[2][2] = -(near + far) / depth;
        frustum._matrix[2][3] = -1.0f;
        
        frustum._matrix[3][2] = -2.0f * near * far / depth;
        frustum._matrix[3][0] = frustum._matrix[3][1] = frustum._matrix[3][3] = 0.0f;

        *this = frustum * *this;
    }
    
    void Matrix::perspective(float fovY, float aspect, float near, float far)
    {
        float height = tanf (fovY / 360.0f * M_PI) * near;
        float width = height * aspect;
        
        frustum (-width, width, -height, height, near, far);
    }
    
    void Matrix::ortho(float left, float right, float bottom, float top, float near, float far)
    {
        float width = right - left;
        float height = top - bottom;
        float depth = far - near;

        if (width == 0.0f || height == 0.0f || depth == 0.0f) { return; }
        
        Matrix    ortho;
        ortho.loadIdentity();
        ortho._matrix[0][0] = 2.0f / width;
        ortho._matrix[3][0] = -(right + left) / width;
        ortho._matrix[1][1] = 2.0f / height;
        ortho._matrix[3][1] = -(top + bottom) / height;
        ortho._matrix[2][2] = -2.0f / depth;
        ortho._matrix[3][2] = -(near + far) / depth;

        *this = ortho * *this;
    }
    
    float* Matrix::asArray()
    {
        return *_matrix;
    }
    
    Matrix Matrix::operator*(const Matrix &other)
    {
        Matrix temp;
        for (int i = 0; i < 4; i++)
        {
            temp._matrix[i][0] = (_matrix[i][0] * other._matrix[0][0]) +
                                 (_matrix[i][1] * other._matrix[1][0]) +
                                 (_matrix[i][2] * other._matrix[2][0]) +
                                 (_matrix[i][3] * other._matrix[3][0]);
            
            temp._matrix[i][1] = (_matrix[i][0] * other._matrix[0][1]) +
                                 (_matrix[i][1] * other._matrix[1][1]) +
                                 (_matrix[i][2] * other._matrix[2][1]) +
                                 (_matrix[i][3] * other._matrix[3][1]);
            
            temp._matrix[i][2] = (_matrix[i][0] * other._matrix[0][2]) +
                                 (_matrix[i][1] * other._matrix[1][2]) +
                                 (_matrix[i][2] * other._matrix[2][2]) +
                                 (_matrix[i][3] * other._matrix[3][2]);
            
            temp._matrix[i][3] = (_matrix[i][0] * other._matrix[0][3]) +
                                 (_matrix[i][1] * other._matrix[1][3]) +
                                 (_matrix[i][2] * other._matrix[2][3]) +
                                 (_matrix[i][3] * other._matrix[3][3]);
        }

        return temp;
    }
    
    Matrix& Matrix::operator=(const Matrix &other)
    {
        _matrix[0][0] = other._matrix[0][0];
        _matrix[0][1] = other._matrix[0][1];
        _matrix[0][2] = other._matrix[0][2];
        _matrix[0][3] = other._matrix[0][3];
        
        _matrix[1][0] = other._matrix[1][0];
        _matrix[1][1] = other._matrix[1][1];
        _matrix[1][2] = other._matrix[1][2];
        _matrix[1][3] = other._matrix[1][3];
        
        _matrix[2][0] = other._matrix[2][0];
        _matrix[2][1] = other._matrix[2][1];
        _matrix[2][2] = other._matrix[2][2];
        _matrix[2][3] = other._matrix[2][3];
        
        _matrix[3][0] = other._matrix[3][0];
        _matrix[3][1] = other._matrix[3][1];
        _matrix[3][2] = other._matrix[3][2];
        _matrix[3][3] = other._matrix[3][3];
        
        return *this;
    }
    
    Matrix& Matrix::operator*=(const Matrix &other)
    {
        *this = *this * other;
        return *this;
    }
}
