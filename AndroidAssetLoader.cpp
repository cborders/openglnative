#include "AssetLoader.h"

#include <android/log.h>
#include <jni.h>
#include <malloc.h>

namespace Moo
{
    // TODO: Go through this and make sure there's no leaks and that we're doing things in a resoonably efficent way
    const char * jstring2string(JNIEnv *env, jstring jStr)
    {
        if (jStr == nullptr) return "";

        const jclass stringClass = env->GetObjectClass(jStr);
        const jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
        const jbyteArray stringJbytes = (jbyteArray) env->CallObjectMethod(jStr, getBytes, env->NewStringUTF("UTF-8"));

        jbyte* pBytes = env->GetByteArrayElements(stringJbytes, nullptr);

        return (const char *)pBytes;
    }

    AssetLoader::AssetLoader(const void *environment)
    {
        _environment = environment;
    }

    const char * AssetLoader::loadShaderFile(const std::string &fileName)
    {
        if (_environment == nullptr)
        {
            __android_log_print(ANDROID_LOG_ERROR, "AssetLoader", "Failed to load assets because of null JNIEnv");
            return nullptr;
        }

        JNIEnv *env = (JNIEnv*)_environment;

        jclass assetLoaderClass = env->FindClass("productions/moo/ctest/assets/AssetLoader");
        if (assetLoaderClass == nullptr)
        {
            __android_log_print(ANDROID_LOG_ERROR, "AssetLoader", "Failed to find AssetLoader class");
            return nullptr;
        }

        jmethodID readFileId = env->GetStaticMethodID(assetLoaderClass, "readFile", "(Ljava/lang/String;)Ljava/lang/String;");
        if(readFileId == nullptr)
        {
            __android_log_print(ANDROID_LOG_ERROR, "AssetLoader", "Failed to find readFile method");
            return nullptr;
        }

        jstring jFileName = env->NewStringUTF(fileName.c_str());

        jstring file = (jstring)env->CallStaticObjectMethod(assetLoaderClass, readFileId, jFileName);

        const char *contents = jstring2string(env, file);

        return contents;
    }

    Texture AssetLoader::loadTexture(const std::string fileName, bool hasAlpha)
    {
        if (_environment == nullptr)
        {
            __android_log_print(ANDROID_LOG_ERROR, "AssetLoader", "Failed to load assets because of null JNIEnv");
            return Texture::errorTexture ();
        }

        JNIEnv *env = (JNIEnv*)_environment;

        jclass assetLoaderClass = env->FindClass("productions/moo/ctest/assets/AssetLoader");
        if (env->ExceptionCheck () || assetLoaderClass == nullptr)
        {
            __android_log_print(ANDROID_LOG_ERROR, "AssetLoader", "Failed to find AssetLoader class");
	        env->ExceptionClear ();
	        return Texture::errorTexture ();
        }

        jmethodID loadTextureId = env->GetStaticMethodID(assetLoaderClass, "loadTexture", "(Ljava/lang/String;Z)Lproductions/moo/ctest/model/Texture;");
        if(env->ExceptionCheck () || loadTextureId == nullptr)
        {
            __android_log_print(ANDROID_LOG_ERROR, "AssetLoader", "Failed to find loadTexture method");
	        env->ExceptionClear ();
	        return Texture::errorTexture ();
        }

        jstring jFileName = env->NewStringUTF(fileName.c_str());
        jobject texture = env->CallStaticObjectMethod(assetLoaderClass, loadTextureId, jFileName, hasAlpha);

	    if(env->ExceptionCheck () || texture == nullptr)
	    {
		    __android_log_print (ANDROID_LOG_ERROR, "AssetLoader", "Failed to load texture %s", fileName.c_str ());
		    env->ExceptionClear ();
		    return Texture::errorTexture ();
	    }

        jclass textureClass = env->GetObjectClass(texture);

        jfieldID pixelsId = env->GetFieldID(textureClass, "pixels", "[I");
        jfieldID widthId = env->GetFieldID(textureClass, "width", "I");
        jfieldID heightId = env->GetFieldID(textureClass, "height", "I");

        int width = env->GetIntField(texture, widthId);
        int height = env->GetIntField(texture, heightId);

        jobject pixelsObject = env->GetObjectField(texture, pixelsId);
        jintArray pixelsArray = reinterpret_cast<jintArray>(pixelsObject);

        int pixelsLength = env->GetArrayLength(pixelsArray);

        jint *pixelInts = env->GetIntArrayElements(pixelsArray, nullptr);
        uint8_t *pixels = (uint8_t*)malloc(pixelsLength);

        for(int i = 0; i < pixelsLength; i++)
        {
            pixels[i] = (uint8_t)pixelInts[i];
        }

        return Texture::loadTexture(pixels, width, height, hasAlpha);
    }
}