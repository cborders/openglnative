#include "Logger.h"

namespace Moo
{
    Logger::Logger() : Logger(std::clog.rdbuf())
    {
    }

    Logger::Logger(std::streambuf *output)
    {
        _oldBuf = std::clog.rdbuf();
        std::clog.rdbuf(output);
    }

    Logger::~Logger()
    {
        std::clog.rdbuf(_oldBuf);
    }

    void Logger::print(const char* s)
    {
        while (s && *s)
        {
            // make sure that there wasn't meant to be more arguments
            if(*s=='%' && *++s!='%')
            {
                // %% represents plain % in a format string
                throw std::runtime_error("invalid format: missing arguments");
            }
            
            std::clog << *s++;
        }
    }

    void Logger::println(const char* s)
    {
        while (s && *s)
        {
            // make sure that there wasn't meant to be more arguments
            if(*s=='%' && *++s!='%')
            {
                // %% represents plain % in a format string
                throw std::runtime_error("invalid format: missing arguments");
            }
            
            std::clog << *s++;
        }
        
        std::clog << "\n";
    }
}
